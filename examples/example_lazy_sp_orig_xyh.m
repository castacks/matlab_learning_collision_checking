%% 
% Copyright (c) 2017 Carnegie Mellon University, Sanjiban Choudhury <sanjibac@andrew.cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%

clc;
clear;
close all;

rng(3);
%% Load data
dataset = strcat(getenv('collision_checking_dataset_folder'), '/dataset_xyh_4/');
set_dataset = strcat(dataset,'set_1/');
do_plot = false;
do_final_path = true;

G = load_graph( strcat(set_dataset,'graph.txt') );
%coll_check_results = dlmread( strcat(set_dataset, 'coll_check_results.txt') );
load( strcat(set_dataset, 'coll_check_results.mat'), 'coll_check_results' );
load(strcat(set_dataset, 'start_goal.mat'));

% Only for 2d viz
env_dataset = strcat(dataset,'environments/');
load(strcat(set_dataset, 'edge_traj_list.mat'), 'edge_traj_list');
id_list = sub2ind(size(G), [edge_traj_list.id1]', [edge_traj_list.id2]');

%% Extract relevant info
coll_check_results = logical(coll_check_results);
edge_check_cost = ones(1, size(coll_check_results,2)); %transpose(full(G(find(G)))); %

%% Load train test id
load(strcat(set_dataset, 'test_id.mat'), 'test_id');

%% Policy
policy = policyLazySPOrig(G, start_idx, goal_idx);

%% Perform stuff
test_world = test_id(98); %just a random world picked from test set with a guarantee that it has a path feasible

load(strcat(env_dataset, 'world_',num2str(test_world),'.mat'), 'map');
figure(1);
visualize_map(map);
pause();


selected_edge_outcome_matrix = [];
path_id = [];
while (1)
    selected_edge = policy.getEdgeToCheck(); % Call policy to select edge
    if (isempty(selected_edge))
        break;
    end
    
    outcome = coll_check_results(test_world, selected_edge); %Observe outcome
    selected_edge_outcome_matrix = [selected_edge_outcome_matrix; selected_edge outcome]; %Update event matrixx
    fprintf('Selected edge : %d Outcome : %d \n', selected_edge, outcome);
    
    if (do_plot)
        figure(1);
        sel_edges = get_edge_from_edgeid( selected_edge_outcome_matrix(:,1), G );
        [~, sel_edges_idx] = ismember(sel_edges, id_list);
        for j = 1:length(sel_edges_idx)
            idx = sel_edges_idx(j);
            if (selected_edge_outcome_matrix(j,2))
                col = 'g';
            else
                col = 'r';
            end
            plot(edge_traj_list(idx).traj(:,1), edge_traj_list(idx).traj(:,2), 'Color', col, 'LineWidth', 3);
        end
        pause;
    end
    
    policy.setOutcome(selected_edge, outcome); %Set outcome to policy
end

fprintf('Num edges checked: %d Cost of check: %f \n', size(selected_edge_outcome_matrix, 1), sum(edge_check_cost(selected_edge_outcome_matrix(:,1))));

%%
figure(1);
cla;
visualize_map(map);
hold on;

if (~isempty(selected_edge_outcome_matrix))
    sel_edges = get_edge_from_edgeid( selected_edge_outcome_matrix(:,1), G );
    [~, sel_edges_idx] = ismember(sel_edges, id_list);
else
    sel_edges_idx = [];
end
for j = 1:length(sel_edges_idx)
    idx = sel_edges_idx(j);
    if (selected_edge_outcome_matrix(j,2))
        col = 'g';
    else
        col = 'r';
    end
    plot(edge_traj_list(idx).traj(:,1), edge_traj_list(idx).traj(:,2), 'Color', col, 'LineWidth', 3);
end

if (do_final_path)
    [~, path] = graphshortestpath(policy.Gplan, policy.start_idx, policy.goal_idx);
    path_edgeids = get_edgeids_from_path(path, G);
    path_edges = get_edge_from_edgeid( path_edgeids, G );
    [~, path_edges_idx] = ismember(path_edges, id_list);
    for j = 1:length(path_edges_idx)
        idx = path_edges_idx(j);
        if (selected_edge_outcome_matrix(j,2))
            col = 'g';
        else
            col = 'r';
        end
        plot(edge_traj_list(idx).traj(:,1), edge_traj_list(idx).traj(:,2), 'm', 'LineWidth', 4);
    end
end
