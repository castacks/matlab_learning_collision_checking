%% 
% Copyright (c) 2017 Carnegie Mellon University, Sanjiban Choudhury <sanjibac@andrew.cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%

clc;
clear;
close all;

%% Load data
dataset = strcat(getenv('collision_checking_dataset_folder'), '/dataset_2d_2/');
set_dataset = strcat(dataset,'set_3/');

G = load_graph( strcat(set_dataset,'graph.txt') );
load(strcat(set_dataset, 'world_library_assignment.mat'), 'world_library_assignment');
load(strcat(set_dataset, 'path_library.mat'), 'path_library');
load( strcat(set_dataset, 'coll_check_results.mat'), 'coll_check_results' );

%% Extract relevant info
world_library_assignment = logical(world_library_assignment);
coll_check_results = logical(coll_check_results);
edge_check_cost = ones(1, size(coll_check_results,2)); %transpose(full(G(find(G)))); %ones(1, size(coll_check_results,2));
%edge_check_cost = transpose(full(G(find(G)))); %ones(1, size(coll_check_results,2));

path_edgeid_map = get_path_edgeid_map( path_library, G );

%% Do a dimensionality reduction
if(isequal(tril(G), triu(G)))
    % Then its undirected and we assume the path forward is the path back
    % and can just check lower triangle of G leading to huge savings
    [ G, coll_check_results, edge_check_cost, path_edgeid_map ] = remove_redundant_edges( G,coll_check_results, edge_check_cost, path_edgeid_map  );
end

%% Load train test id
load(strcat(set_dataset, 'train_id.mat'), 'train_id');
load(strcat(set_dataset, 'test_id.mat'), 'test_id');

train_world_library_assignment = world_library_assignment(train_id, :);
train_coll_check_results = coll_check_results(train_id, :);
test_coll_check_results = coll_check_results(test_id, :);

%% Create 0.5 bernoulli r.v dataset
unbiased_coll_check_results = [];
for i = 1:size(test_coll_check_results,1)
    is_valid = 0;
    while (~is_valid)
        outcome_matrix = [transpose(1:size(coll_check_results,2)) randi(2,size(coll_check_results,2),1)-1];
        is_valid = any_path_feasible( path_edgeid_map, outcome_matrix );
        if (is_valid)
            unbiased_coll_check_results = [unbiased_coll_check_results; outcome_matrix(:,2)'];
        end
    end
end

%% Create a policy set
policy_set = {};
policy_set{length(policy_set)+1} = @() policyLazySP(path_edgeid_map, train_world_library_assignment, train_coll_check_results, G, 0.01, 0);
load(strcat(set_dataset, 'saved_decision_trees/drd_decision_tree_data.mat'), 'decision_tree_data');
policy_set{length(policy_set)+1} = @() policyDecisionTreeandBern(decision_tree_data, path_edgeid_map, edge_check_cost, 0.01, false, 0.2);

%% Do stuff
biased_cost_set = zeros(length(policy_set), length(test_id));
unbiased_cost_set = zeros(length(policy_set), length(test_id));

for i = 1:length(policy_set)
    parfor j = 1:length(test_id)
        policy_fn = policy_set{i};
        policy = policy_fn();
        selected_edge_outcome_matrix = [];
        while (1)
            selected_edge = policy.getEdgeToCheck(); % Call policy to select edge
            if (isempty(selected_edge))
                error('No valid selection made'); % Invalid selection made
            end
            
            outcome = test_coll_check_results(j, selected_edge); %Observe outcome

            policy.setOutcome(selected_edge, outcome); %Set outcome to policy
            selected_edge_outcome_matrix = [selected_edge_outcome_matrix; selected_edge outcome]; %Update event matrixx
            
            if (any_path_feasible( path_edgeid_map, selected_edge_outcome_matrix ))
                break;
            end
        end
        biased_cost_set(i, j) = sum(edge_check_cost(selected_edge_outcome_matrix(:,1)));
        fprintf('Policy: %d Test: %d Cost of check: %f \n', i, j, biased_cost_set(i, j));
    end
end


for i = 1:length(policy_set)
    parfor j = 1:length(test_id)
        policy_fn = policy_set{i};
        policy = policy_fn();
        selected_edge_outcome_matrix = [];
        while (1)
            selected_edge = policy.getEdgeToCheck(); % Call policy to select edge
            if (isempty(selected_edge))
                error('No valid selection made'); % Invalid selection made
            end
            
            outcome = unbiased_coll_check_results(j, selected_edge); %Observe outcome

            policy.setOutcome(selected_edge, outcome); %Set outcome to policy
            selected_edge_outcome_matrix = [selected_edge_outcome_matrix; selected_edge outcome]; %Update event matrixx
            
            if (any_path_feasible( path_edgeid_map, selected_edge_outcome_matrix ))
                break;
            end
        end
        unbiased_cost_set(i, j) = sum(edge_check_cost(selected_edge_outcome_matrix(:,1)));
        fprintf('Policy: %d Test: %d Cost of check: %f \n', i, j, unbiased_cost_set(i, j));
    end
end

% 
% 
% %% Create a policy set
% policy_lazysp = @() policyLazySP(path_edgeid_map, train_world_library_assignment, train_coll_check_results, G, 0.01, 0);
% load(strcat(set_dataset, 'saved_decision_trees/drd_decision_tree_data.mat'), 'decision_tree_data');
% policy_direct_bisect = @() policyDecisionTreeandBern(decision_tree_data, path_edgeid_map, edge_check_cost, 0.01, false, 0.2);
% 
% %% Perform stuff
% alpha_set = linspace(0,size(test_coll_check_results,1),6);
% direct_cost_set = zeros(length(alpha_set), size(test_coll_check_results,1));
% lazysp_cost_set = zeros(length(alpha_set), size(test_coll_check_results,1));
% for i = 1:length(alpha_set)
%     alpha = alpha_set(i);
%     blended_coll_check_results = [unbiased_coll_check_results(randperm(size(test_coll_check_results,1), alpha), :); 
%                                   test_coll_check_results(randperm(size(test_coll_check_results,1), size(test_coll_check_results,1) - alpha), :)];
%     parfor j = 1:size(blended_coll_check_results,1)
%         policy = policy_direct_bisect();
%         selected_edge_outcome_matrix = [];
%         while (1)
%             selected_edge = policy.getEdgeToCheck(); % Call policy to select edge
%             if (isempty(selected_edge))
%                 error('No valid selection made'); % Invalid selection made
%             end
% 
%             outcome = blended_coll_check_results(j, selected_edge); %Observe outcome
%             policy.setOutcome(selected_edge, outcome); %Set outcome to policy
%             selected_edge_outcome_matrix = [selected_edge_outcome_matrix; selected_edge outcome]; %Update event matrixx
% 
%             if (any_path_feasible( path_edgeid_map, selected_edge_outcome_matrix ))
%                 break;
%             end
%         end
%         direct_cost_set(i, j) = sum(edge_check_cost(selected_edge_outcome_matrix(:,1)));
%         fprintf('Policy: DIRECT Alpha: %d Test: %d Cost of check: %f \n', i, j, direct_cost_set(i, j));
%     end
% 
%     parfor j = 1:size(blended_coll_check_results,1)
%         policy = policy_lazysp();
%         selected_edge_outcome_matrix = [];
%         while (1)
%             selected_edge = policy.getEdgeToCheck(); % Call policy to select edge
%             if (isempty(selected_edge))
%                 error('No valid selection made'); % Invalid selection made
%             end
% 
%             outcome = blended_coll_check_results(j, selected_edge); %Observe outcome
%             policy.setOutcome(selected_edge, outcome); %Set outcome to policy
%             selected_edge_outcome_matrix = [selected_edge_outcome_matrix; selected_edge outcome]; %Update event matrixx
% 
%             if (any_path_feasible( path_edgeid_map, selected_edge_outcome_matrix ))
%                 break;
%             end
%         end
%         lazysp_cost_set(i, j) = sum(edge_check_cost(selected_edge_outcome_matrix(:,1)));
%         fprintf('Policy: LAZYSP Alpha: %d Test: %d Cost of check: %f \n', i, j, lazysp_cost_set(i, j));
%     end
% end