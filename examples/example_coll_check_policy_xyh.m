%% 
% Copyright (c) 2017 Carnegie Mellon University, Sanjiban Choudhury <sanjibac@andrew.cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%

clc;
clear;
close all;

rng(3);

%% Options
do_plot = false;
do_likelihood_plot = false;
do_final_path = true;
do_save = false;

%% Load data
dataset = strcat(getenv('collision_checking_dataset_folder'), '/dataset_xyh_4/');
set_dataset = strcat(dataset,'set_1/');

G = load_graph( strcat(set_dataset,'graph.txt') );
load(strcat(set_dataset, 'world_library_assignment.mat'), 'world_library_assignment');
load(strcat(set_dataset, 'path_library.mat'), 'path_library');
load( strcat(set_dataset, 'coll_check_results.mat'), 'coll_check_results' );

% Only for 2d viz
env_dataset = strcat(dataset,'environments/');
load(strcat(set_dataset, 'edge_traj_list.mat'), 'edge_traj_list');
id_list = sub2ind(size(G), [edge_traj_list.id1]', [edge_traj_list.id2]');
all_edges = get_edge_from_edgeid( transpose(1:size(coll_check_results,2)), G );
[~, all_edges_idx] = ismember(all_edges, id_list);
%% Extract relevant info
world_library_assignment = logical(world_library_assignment);
coll_check_results = logical(coll_check_results);
edge_check_cost = ones(1, size(coll_check_results,2)); %transpose(full(G(find(G)))); %
path_edgeid_map = get_path_edgeid_map( path_library, G );

%% Do a dimensionality reduction
if(isequal(tril(G), triu(G)))
    % Then its undirected and we assume the path forward is the path back
    % and can just check lower triangle of G leading to huge savings
    [ G, coll_check_results, edge_check_cost, path_edgeid_map ] = remove_redundant_edges( G,coll_check_results, edge_check_cost, path_edgeid_map  );
end

%% Load train test id
load(strcat(set_dataset, 'train_id.mat'), 'train_id');
load(strcat(set_dataset, 'test_id.mat'), 'test_id');

train_world_library_assignment = world_library_assignment(train_id, :);
train_coll_check_results = coll_check_results(train_id, :);

%% Select a policy

option_policy = 9;
switch(option_policy)
    case 1
        policy = policyRandomEdge(path_edgeid_map, train_world_library_assignment, train_coll_check_results, 0.01, false, 0);
    case 2
        policy = policyRandomEdge(path_edgeid_map, train_world_library_assignment, train_coll_check_results, 0.01, false, 1);
    case 3
        policy = policyMaxTallyEdge(path_edgeid_map, train_world_library_assignment, train_coll_check_results, 0.01, false, 0);
    case 4
        policy = policyMaxTallyEdge(path_edgeid_map, train_world_library_assignment, train_coll_check_results, 0.01, false, 1);
    case 5
        policy = policyMaxSetCover(path_edgeid_map, train_world_library_assignment, train_coll_check_results, 0.01, false, 0);
    case 6
        policy = policyMaxSetCover(path_edgeid_map, train_world_library_assignment, train_coll_check_results, 0.01, false, 1);
    case 7
        policy = policyMaxMVOI(path_edgeid_map, train_world_library_assignment, train_coll_check_results, 0.01, false);
    case 8
        policy = policyDRDBernoulli(path_edgeid_map, edge_check_cost, train_world_library_assignment, train_coll_check_results, 0.01, false, 0);
    case 9 %BISECT
        policy = policyDRDBernoulli(path_edgeid_map, edge_check_cost, train_world_library_assignment, train_coll_check_results, 0.01, false, 1);
    case 10 %DIRECT
        load(strcat(set_dataset, 'saved_decision_trees/drd_decision_tree_data.mat'), 'decision_tree_data');
        policy = policyDecisionTreeandBern(decision_tree_data, path_edgeid_map, edge_check_cost, 0.01, false, 0.2);
    case 11 %LAZYSP
        policy = policyLazySP(path_edgeid_map, train_world_library_assignment, train_coll_check_results, G, 0.01, 0);
end
%% Perform stuff
test_world = test_id(98); %just a random world picked from test set with a guarantee that it has a path feasible

load(strcat(env_dataset, 'world_',num2str(test_world),'.mat'), 'map');
figure(1);
visualize_map(map);
pause();

selected_edge_outcome_matrix = [];
sel_edges_idx = [];
path_id = [];

while (1)
    selected_edge = policy.getEdgeToCheck(); % Call policy to select edge
    if (isempty(selected_edge))
        error('No valid selection made'); % Invalid selection made
    end
    
    outcome = coll_check_results(test_world, selected_edge); %Observe outcome
    fprintf('Selected edge : %d Outcome : %d \n', selected_edge, outcome);
    
    if (do_plot)
        figure(1);
        cla;
        visualize_map(map);
        hold on;
        
        edge_likelihood = policy.getEdgeLikelihood();
        cmap = flipud(cool);
        for i = 1:length(all_edges_idx)
            idx = all_edges_idx(i);
            v = edge_likelihood(i);
            if (v < 1e-3)
                continue;
            end
            plot(edge_traj_list(idx).traj(:,1), edge_traj_list(idx).traj(:,2), 'Color', get_color_interp(cmap, v), 'LineWidth', 2*v);
        end
        
        if (~isempty(selected_edge_outcome_matrix))
            sel_edges = get_edge_from_edgeid( selected_edge_outcome_matrix(:,1), G );
            [~, sel_edges_idx] = ismember(sel_edges, id_list);
        else
            sel_edges_idx = [];
        end
        for j = 1:length(sel_edges_idx)
            idx = sel_edges_idx(j);
            if (selected_edge_outcome_matrix(j,2))
                col = 'g';
            else
                col = 'r';
            end
            plot(edge_traj_list(idx).traj(:,1), edge_traj_list(idx).traj(:,2), 'Color', col, 'LineWidth', 3);
        end
        pause;
        if (do_save)
            saveas(gcf, strcat(num2str(size(selected_edge_outcome_matrix,1)+1),'.pdf'));
        end
    end
    
    selected_edge_outcome_matrix = [selected_edge_outcome_matrix; selected_edge outcome]; %Update event matrix
    policy.setOutcome(selected_edge, outcome); %Set outcome to policy
    [done, path_id] = any_path_feasible( path_edgeid_map, selected_edge_outcome_matrix );
    
    if (done)
        break;
    end
end

%%
figure(1);
cla;
visualize_map(map);
hold on;
if (do_likelihood_plot)
    edge_likelihood = policy.getEdgeLikelihood();
    cmap = flipud(cool);
    for i = 1:length(all_edges_idx)
        idx = all_edges_idx(i);
        v = edge_likelihood(i);
        if (v < 1e-3)
            continue;
        end
        plot(edge_traj_list(idx).traj(:,1), edge_traj_list(idx).traj(:,2), 'Color', get_color_interp(cmap, v), 'LineWidth', 2*v);
    end
end

if (~isempty(selected_edge_outcome_matrix))
    sel_edges = get_edge_from_edgeid( selected_edge_outcome_matrix(:,1), G );
    [~, sel_edges_idx] = ismember(sel_edges, id_list);
else
    sel_edges_idx = [];
end
for j = 1:length(sel_edges_idx)
    idx = sel_edges_idx(j);
    if (selected_edge_outcome_matrix(j,2))
        col = 'g';
    else
        col = 'r';
    end
    plot(edge_traj_list(idx).traj(:,1), edge_traj_list(idx).traj(:,2), 'Color', col, 'LineWidth', 3);
end

if (do_final_path)
    path_edgeids = path_edgeid_map{path_id};
    path_edges = get_edge_from_edgeid( path_edgeids, G );
    [~, path_edges_idx] = ismember(path_edges, id_list);
    for j = 1:length(path_edges_idx)
        idx = path_edges_idx(j);
        if (selected_edge_outcome_matrix(j,2))
            col = 'g';
        else
            col = 'r';
        end
        plot(edge_traj_list(idx).traj(:,1), edge_traj_list(idx).traj(:,2), 'm', 'LineWidth', 4);
    end
end

fprintf('Num edges checked: %d Cost of check: %f \n', size(selected_edge_outcome_matrix, 1), sum(edge_check_cost(selected_edge_outcome_matrix(:,1))));

