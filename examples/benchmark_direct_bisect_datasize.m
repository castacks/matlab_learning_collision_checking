%% 
% Copyright (c) 2017 Carnegie Mellon University, Sanjiban Choudhury <sanjibac@andrew.cmu.edu>
%
% For License information please see the LICENSE file in the root directory.
%

clc;
clear;
close all;

%% Load data
dataset = strcat(getenv('collision_checking_dataset_folder'), '/dataset_2d_6/');
set_dataset = strcat(dataset,'set_1/');

G = load_graph( strcat(set_dataset,'graph.txt') );
load(strcat(set_dataset, 'world_library_assignment.mat'), 'world_library_assignment');
load(strcat(set_dataset, 'path_library.mat'), 'path_library');
load( strcat(set_dataset, 'coll_check_results.mat'), 'coll_check_results' );

%% Extract relevant info
world_library_assignment = logical(world_library_assignment);
coll_check_results = logical(coll_check_results);
edge_check_cost = ones(1, size(coll_check_results,2)); %transpose(full(G(find(G)))); %
path_edgeid_map = get_path_edgeid_map( path_library, G );

%% Do a dimensionality reduction
if(isequal(tril(G), triu(G)))
    % Then its undirected and we assume the path forward is the path back
    % and can just check lower triangle of G leading to huge savings
    [ G, coll_check_results, edge_check_cost, path_edgeid_map ] = remove_redundant_edges( G,coll_check_results, edge_check_cost, path_edgeid_map  );
end

%% Load train id
load(strcat(set_dataset, 'train_id.mat'), 'train_id');
load(strcat(set_dataset, 'test_id.mat'), 'test_id');
train_id = 1:1000;

%% Get fraction of train
train_frac_set = [0.05 0.1 0.2 0.4 0.6 0.8 1.0];%[0.3 0.4 0.6 0.7 0.8 0.9];%0.1:0.1:1.0;
cumulative_cost_set = zeros(length(train_frac_set), length(test_id));

for i = 1:length(train_frac_set)
    train_frac = train_frac_set(i);
%    idx = randperm(length(train_id), round(train_frac * length(train_id)));
    actual_train_id = train_id(1:round(train_frac * length(train_id)));
    
    train_world_library_assignment = world_library_assignment(actual_train_id, :);
    train_coll_check_results = coll_check_results(actual_train_id, :);
    
    %% Select a policy to create DRD
    drd_policy = policyIncDRD(train_world_library_assignment, train_coll_check_results, edge_check_cost, path_edgeid_map, 5);
    decision_tree_thresh = (10/length(actual_train_id));
    
    %% Create decision tree
    % Initialize decision tree
    root_data = drd_policy.get_decision_tree_data();
    decision_tree = initialize_decision_tree (root_data, 2);
    policy_node_set = struct('policy', drd_policy, 'node', 1);
    
    while (~isempty(policy_node_set))
        policy_node_set_new = [];
        for policy_node = policy_node_set
            parent_node = policy_node.node;
            parent_data = get_data_from_decision_tree(parent_node, decision_tree);
            for outcome_id = [1 2]
                if (outcome_id == 1)
                    outcome = false;
                else
                    outcome = true;
                end
                policy = copy(policy_node.policy);
                policy.setOutcome(parent_data.selected_edge, outcome);
                
                [decision_tree, child_node] = add_child_to_decision_tree( decision_tree, parent_node, outcome_id, policy.get_decision_tree_data() );
                if (policy.active_prob() >= decision_tree_thresh)
                    fprintf('Adding node %d Prob %f \n', child_node, policy.active_prob());
                    policy_node_set_new = [policy_node_set_new struct('policy', policy, 'node', child_node)];
                end
            end
        end
        policy_node_set = policy_node_set_new;
    end
    
    decision_tree_data.hyp_test = train_coll_check_results;
    decision_tree_data.hyp_region = world_library_assignment;
    decision_tree_data.decision_tree = decision_tree;
    
    rng(1);
    parfor j = 1:length(test_id)
        policy = policyDecisionTreeandBern(decision_tree_data, path_edgeid_map, edge_check_cost, 0.01, false, 0.0);
        test_world = test_id(j);
        selected_edge_outcome_matrix = [];
        while (1)
            selected_edge = policy.getEdgeToCheck(); % Call policy to select edge
            if (isempty(selected_edge))
                error('No valid selection made'); % Invalid selection made
            end
            
            outcome = coll_check_results(test_world, selected_edge); %Observe outcome
            policy.setOutcome(selected_edge, outcome); %Set outcome to policy
            selected_edge_outcome_matrix = [selected_edge_outcome_matrix; selected_edge outcome]; %Update event matrixx
            
            if (any_path_feasible( path_edgeid_map, selected_edge_outcome_matrix ))
                break;
            end
        end
        cumulative_cost_set(i, j) = sum(edge_check_cost(selected_edge_outcome_matrix(:,1)));
        fprintf('Policy: %d Test: %d Cost of check: %f \n', i, j, cumulative_cost_set(i, j));
    end
end