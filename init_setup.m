%% Set up the workspace with relevant paths 
addpath(genpath(strcat(pwd,'/utils')));
addpath(genpath(strcat(pwd,'/collision_checking_policies')));
setenv('collision_checking_dataset_folder','/Volumes/NO NAME/Dropbox/current/datasets/matlab_learning_collision_checking_dataset')
%setenv('collision_checking_dataset_folder','/media/aeroscout/03ED-079C/Dropbox/current/datasets/matlab_learning_collision_checking_dataset')
