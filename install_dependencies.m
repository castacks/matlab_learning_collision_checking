old_folder = cd('../matlab_environment_generation/');
run init_setup;
cd(old_folder);

old_folder = cd('../matlab_graph_utils/');
run init_setup;
cd(old_folder);

old_folder = cd('../matlab_decision_tree_utils/');
run init_setup;
cd(old_folder);

old_folder = cd('../matlab_decision_region_determination/');
run init_setup;
cd(old_folder);

old_folder = cd('../matlab_drd_bernoulli/');
run init_setup;
cd(old_folder);

old_folder = cd('../matlab_state_lattice/');
run init_setup;
cd(old_folder);

old_folder = cd('../matlab_circular_curve/');
run init_setup;
cd(old_folder);